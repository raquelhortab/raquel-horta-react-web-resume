import './SkillBar.css';
function SkillBar({percentage}){
    return(<div className={'skill-bar-outer'}>
        <div className={'skill-bar-inner'} style={{width:`${percentage}%`}}></div>
    </div>);
}
export default SkillBar;