import './SkillList.css';
import SkillBar from './SkillBar';

function SkillItem({text, percentage, id = '', description}){
    return(<div className={`skill-item ${id}`}>
        <div><span className={'skill-name'}>{text}</span> {description}</div> {percentage && <SkillBar percentage={percentage}/>}
    </div>);
}

export default SkillItem;